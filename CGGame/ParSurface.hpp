#pragma once
//
// Parametric Surfaces
//

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include <math.h>

inline glm::vec3 getSphericalPos(float u, float v, float r) {
	u *= 2.0f * M_PI;
	v *= (float)M_PI;

	float su = sinf(u), sv = sinf(v);
	float cu = cosf(u), cv = cosf(v);

	return glm::vec3(r * cu * sv, r * cv, r * su * sv);
}

inline glm::vec3 getSphericalNorm(float u, float v) {
	u *= (float)2.0 * M_PI;
	v *= (float)M_PI;

	float su = sinf(u), sv = sinf(v);
	float cu = cosf(u), cv = cosf(v);

	return glm::vec3(cu * sv, cv, su * sv);
}

inline glm::vec2 getSphericalTex(float u, float v) {
	return glm::vec2(1 - u, 1 - v);
}