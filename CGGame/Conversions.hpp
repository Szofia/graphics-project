#pragma once

// GLM
#include <glm\glm.hpp>

//
// We restrict SetUniform() function to use supported types only
//
template <typename T>
using IsValidBaseType = std::enable_if_t<
	std::is_same_v<T, GLint> ||
	std::is_same_v<T, GLuint> ||
	std::is_same_v<T, GLfloat> ||
	std::is_same_v<T, GLdouble>
>;

// GLM Vector types
template <typename T, glm::precision P = glm::defaultp>
using IsValidGLMVectorType = std::enable_if_t<
	std::is_base_of_v<glm::tvec1<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tvec2<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tvec3<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tvec4<typename T::value_type, P>, T>
>;

// GLM Matrix types
template <typename T, glm::precision P = glm::defaultp>
using IsValidGLMMatrixType = std::enable_if_t<
	std::is_base_of_v<glm::tmat2x2<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat2x3<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat2x4<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat3x2<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat3x4<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat3x4<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat4x2<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat4x3<typename T::value_type, P>, T> ||
	std::is_base_of_v<glm::tmat4x4<typename T::value_type, P>, T>
>;

//
// Provide for tvec<*,*>::length, cols, rows
//
template<typename T>	constexpr std::pair<size_t, size_t> ComponentCount() { return std::make_pair(1, 1); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::vec2>() { return std::make_pair(2, 1); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::vec3>() { return std::make_pair(3, 1); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::vec4>() { return std::make_pair(4, 1); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat2>() { return std::make_pair(2, 2); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat3>() { return std::make_pair(3, 3); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat4>() { return std::make_pair(4, 4); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat4x3>() { return std::make_pair(4, 3); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat4x2>() { return std::make_pair(4, 2); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat3x4>() { return std::make_pair(3, 4); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat3x2>() { return std::make_pair(3, 2); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat2x4>() { return std::make_pair(2, 4); }
template<> constexpr std::pair<size_t, size_t> ComponentCount<glm::mat2x3>() { return std::make_pair(2, 3); }

//
// Selection of the appropriate glUniform function
//
template <typename T, int N, int M> inline void CallSetter(GLint, GLsizei, const T*) { }

// Vectors
template <> inline void CallSetter<GLint, 1, 1>(GLint p_loc, GLsizei p_count, const GLint* p_arg) { glUniform1iv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLint, 2, 1>(GLint p_loc, GLsizei p_count, const GLint* p_arg) { glUniform2iv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLint, 3, 1>(GLint p_loc, GLsizei p_count, const GLint* p_arg) { glUniform3iv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLint, 4, 1>(GLint p_loc, GLsizei p_count, const GLint* p_arg) { glUniform4iv(p_loc, p_count, p_arg); }

template <> inline void CallSetter<GLuint, 1, 1>(GLint p_loc, GLsizei p_count, const GLuint* p_arg) { glUniform1uiv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLuint, 2, 1>(GLint p_loc, GLsizei p_count, const GLuint* p_arg) { glUniform2uiv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLuint, 3, 1>(GLint p_loc, GLsizei p_count, const GLuint* p_arg) { glUniform3uiv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLuint, 4, 1>(GLint p_loc, GLsizei p_count, const GLuint* p_arg) { glUniform4uiv(p_loc, p_count, p_arg); }

template <> inline void CallSetter<GLfloat, 1, 1>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniform1fv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLfloat, 2, 1>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniform2fv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLfloat, 3, 1>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniform3fv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLfloat, 4, 1>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniform4fv(p_loc, p_count, p_arg); }

template <> inline void CallSetter<GLdouble, 1, 1>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniform1dv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLdouble, 2, 1>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniform2dv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLdouble, 3, 1>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniform3dv(p_loc, p_count, p_arg); }
template <> inline void CallSetter<GLdouble, 4, 1>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniform4dv(p_loc, p_count, p_arg); }

//Matrices
template <> inline void CallSetter<GLfloat, 4, 4>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix4fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 3, 3>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix3fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 2, 2>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix2fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 4, 3>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix4x3fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 4, 2>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix4x2fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 3, 4>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix3x4fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 3, 2>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix3x2fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 2, 4>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix2x4fv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLfloat, 2, 3>(GLint p_loc, GLsizei p_count, const GLfloat* p_arg) { glUniformMatrix2x3fv(p_loc, p_count, GL_FALSE, p_arg); }

template <> inline void CallSetter<GLdouble, 4, 4>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix4dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 3, 3>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix3dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 2, 2>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix2dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 4, 3>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix4x3dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 4, 2>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix4x2dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 3, 4>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix3x4dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 3, 2>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix3x2dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 2, 4>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix2x4dv(p_loc, p_count, GL_FALSE, p_arg); }
template <> inline void CallSetter<GLdouble, 2, 3>(GLint p_loc, GLsizei p_count, const GLdouble* p_arg) { glUniformMatrix2x3dv(p_loc, p_count, GL_FALSE, p_arg); }

//
// Information of the type of the element. if it is a scalar, it means the type of the argument
// If it is a container, this boils down to the type of the container elements
//
template <typename T> struct ElementInfo { using value_type = T; };
template <typename T> struct ElementInfo<std::vector<T>> { using value_type = T; };
template <typename T, size_t N> struct ElementInfo<std::array<T, N>> { using value_type = T; };
template <typename T, size_t N> struct ElementInfo<T[N]> { using value_type = T; };

//
// We extract the primitive type from the construct we received
//
template <typename T, typename C = void> struct ExtractPrimitiveType;
template <typename T> struct ExtractPrimitiveType<T, IsValidBaseType<T>> { using primitive_type = T; };
template <typename T> struct ExtractPrimitiveType<T, IsValidGLMVectorType<T>> { using primitive_type =  typename T::value_type; };
template <typename T> struct ExtractPrimitiveType<T, IsValidGLMMatrixType<T>> { using primitive_type = typename T::value_type; };

//
// Get the length of a container in elements
//
template <typename T>			constexpr GLsizei ContainerLength(const T&) { return 1; }
template <typename T, size_t N> constexpr GLsizei ContainerLength(const std::array<T, N>&) { return N; }
template <typename T, size_t N> constexpr GLsizei ContainerLength(const T(&)[N]) { return N; }
template <typename T>			constexpr GLsizei ContainerLength(const std::vector<T>& p_arr) { return (GLsizei)p_arr.size(); }

// Get pointer of the beginning of the container
template <typename T>			constexpr const T* PointerToStart(const T& p_arr) { return &p_arr; }
template <typename T, size_t N> constexpr const T* PointerToStart(const std::array<T, N>& p_arr) { return &p_arr[0]; }
template <typename T, size_t N> constexpr const T* PointerToStart(const T(&p_arr)[N]) { return &p_arr[0]; }
template <typename T>			constexpr const T* PointerToStart(const std::vector<T>& p_arr) { return p_arr.empty() ? nullptr : &p_arr[0]; }