#include <iostream>
#include <sstream>

#include "GameApp.h"

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

void exitProgram() {
	SDL_Quit();

	std::cout << "Press ENTER to exit..." << std::endl;
	std::cin.get();
}

int main(int argc, char* args[]) {

	// Call atExit() before the program exits
	atexit( exitProgram );

	//
	// 1.: Initialize SDL
	//
	// Call SDL subsystems: audio and video
	if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) != 0) {
		SDL_Log("[Starting SDL] Error: Unable to initialize SDL: %s", SDL_GetError());
		return 1;
	}

	//
	// 2.: Set OpenGL window attributes, window creation, start OpenGL
	//
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	
	// set buffer sizes, doublepuffer, depth puffer size
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	//anti-aliasing?

	SDL_Window *win = NULL;
	win = SDL_CreateWindow("Pusheen - Find your recipes!",
		100,
		100,
		640,
		480,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
	);

	// if window fails to be created
	if (win == NULL) {
		SDL_Log("[Starting SDL Window] Error: Unable to create SDL window: %s", SDL_GetError());
		return 1;
	}

	//
	// 3.: Create OpenGL Context
	//
	SDL_GLContext	context = SDL_GL_CreateContext(win);
	if ( context == NULL ) {
		SDL_Log("[Creating OpenGL Context] Error: Unable to create OpenGL context: %s", SDL_GetError());
		return 1;
	}

	// syncronize with vertical retrace (wait for vsync)
	SDL_GL_SetSwapInterval(1);

	// start GLEW
	GLenum error = glewInit();
	if (error != GLEW_OK) {
		SDL_Log("[GLEW] Error: Unable to start GLEW");
		return 1;
	}

	// get OpenGL version
	int glVersion[2] = { -1, -1 };
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
	SDL_Log("[OPENGL VERSION] Running OpenGL %d.%d", glVersion[0], glVersion[1]);
	
	// at error delete context and destroy window
	if (glVersion[0] == -1 && glVersion[1] == -1) {
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(win);

		SDL_Log("[Creating OpenGL Context] Error: Unable to get version of OpenGL. Unable to create OpenGL context. Check SDL_GL_SetAttribute(...) calls.");
		return 1;
	}

	// set title
	std::stringstream window_title;
	window_title << "Pusheen - Find your recipes! - OpenGL " << glVersion[0] << "." << glVersion[1];
	SDL_SetWindowTitle(win, window_title.str().c_str());

	//
	// 4.: Events & Renderer
	//
	SDL_Event ev;
	bool quitGame = false;
	GameApp app;

	if (!app.Init()) {
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(win);
		
		SDL_Log("[App Init] Error: Application initialization failed.");

		return 1;
	}

	while (!quitGame){
		while (SDL_PollEvent(&ev)) {
			switch (ev.type) {
				case SDL_QUIT:
					quitGame = true;
					break;
				case SDL_KEYDOWN:
					if (ev.key.keysym.sym == SDLK_ESCAPE) {
						quitGame = true;
						app.KeyboardDown(ev.key);
					}
					break;
				case SDL_KEYUP:
					app.KeyboardUp(ev.key);
					break;
				case SDL_MOUSEBUTTONDOWN:
					app.MouseDown(ev.button);
					break;
				case SDL_MOUSEBUTTONUP:
					app.MouseUp(ev.button);
					break;
				case SDL_MOUSEWHEEL:
					app.MouseWheel(ev.wheel);
					break;
				case SDL_MOUSEMOTION:
					app.MouseMove(ev.motion);
					break;
				case SDL_WINDOWEVENT:
					if (ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
						app.Resize(ev.window.data1, ev.window.data2);
					}
					break;
			}
		}

		app.Update();
		app.Render();

		// update a window with OpenGL rendering
		SDL_GL_SwapWindow(win);
	}

	//
	// 5.: Exit program
	// 

	// clean app, context, window
	app.Clean();

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(win);

	return 0;
}