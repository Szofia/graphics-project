#pragma once

//
// Camera Model - how the user looks at the scene
//

#include <SDL.h>
#include <glm/glm.hpp>

class Camera {
public:
	Camera(void);
	Camera(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up);
	~Camera(void);
	
	glm::mat4 GetViewMatrix();
	void Update(float _deltaTime);

	void SetView(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up);
	void SetProj(float _angle, float _aspect, float _zn, float _zf);
	void LookAt(glm::vec3 _at);

	void SetSpeed(float _val);
	glm::vec3 GetEye()
	{
		return eye;
	}

	glm::vec3 GetAt()
	{
		return at;
	}

	glm::vec3 GetUp()
	{
		return up;
	}

	glm::mat4 GetProj()
	{
		return m_matProj;
	}

	glm::mat4 GetViewProj()
	{
		return m_matViewProj;
	}

	void Resize(int _w, int _h);

	void KeyboardDown(SDL_KeyboardEvent& key);
	void KeyboardUp(SDL_KeyboardEvent& key);
	void MouseMove(SDL_MouseMotionEvent& mouse);

private:
	glm::vec3	eye;
	glm::vec3	up;
	glm::vec3	at;

	/// Spherical coordinate pair (u,v) denoting the
	// current viewing direction from the view position m_eye. 
	float	camU;
	float	camV;

	/// Updates the UV.
	/// The du, i.e. the change of spherical coordinate u.
	///The dv, i.e. the change of spherical coordinate v.
	void UpdateUV(float du, float dv);

	///  The traversal speed of the camera
	float		m_speed;
	/// The view matrix of the camera
	glm::mat4	m_viewMatrix;

	glm::mat4	m_matViewProj;

	bool	m_slow;



	/// The distance of the look at point from the camera. 
	float	camDist;
	/// The unit vector pointing towards the viewing direction.
	glm::vec3	camViewDir;
	/// The unit vector pointing to the 'right'
	// (positive x-axis of the camera space)
	glm::vec3	camRight;

	glm::mat4	m_matProj;

	float	goFw;
	float	goRight;
};