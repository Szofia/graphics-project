#pragma once
//
// OpenGL Application
//

#include "ProgramObject.h"
#include "Camera.h"
#include "Mesh.h"
#include "OBJParser.h"

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

class GameApp {
public:
	GameApp(void);
	~GameApp(void);

	bool Init();
	void Clean();
	void Update();
	void Render();

	void InitSphere();
	void InitShaders();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);
protected:
	ProgramObject program_;
	// shaderprogram ID
	GLuint m_programID;

	// VAO / VBO / IndexBuffer ID
	GLuint m_vaoID;
	GLuint m_vboID;
	GLuint m_ibID;

	// texture ID
	GLuint m_randTexID;


	// transformation matrices
	glm::mat4 mWorld;
	glm::mat4 mWorldInv;
	glm::mat4 mView;
	glm::mat4 mProj;

	// locations
	GLuint loc_mvp;
	GLuint loc_world;
	GLuint loc_worldInv;
	GLuint loc_viewPos;
	GLuint loc_tex;

	// position and color vertex
	struct Vertex
	{
		glm::vec3 p;
		glm::vec3 n;
		glm::vec2 t;
	};
	
	// camera
	Camera camera;

	// mesh
	Mesh *mesh;

	// for parametric surfaces
	static const int N	= 20;
	static const int M	= 10;
};