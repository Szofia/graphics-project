#version 140

in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex0;

out vec4 fs_out_col;

// eye direction
uniform vec3 viewPos;

// light direction
uniform vec3 light_dir = vec3(1, 1, -0.7);

// light prop. ambient, diffuse, ...
uniform vec3 La = vec3(0.8, 0.8, 0.8);
uniform vec3 Ld = vec3(0.4, 0.6, 0.6);
uniform vec3 Ls = vec3(0.3, 0.3, 0.3);

// material prop. ambient, diffuse, ...
uniform vec3 Ma = vec3(0.2, 0.4, 0.6);
uniform vec3 Md = vec3(0.2, 0.4, 0.6);
uniform vec3 Ms = vec3(0.2, 0.4, 0.6);

// specular intensity
int specular_power = 2;

uniform sampler2D randomTexture;

void main()
{
	// Ambient
	vec3 ambient = La * Ma;

	// Diffuse
	vec3 normalized = normalize(vs_out_norm);
	float diffImpact = max(dot(normalized, light_dir), 0.0f);
	
	vec3 diffuse = diffImpact * Md;

	// Phong Model
	vec3 viewDir = normalize(viewPos - vs_out_pos);
	vec3 reflectDir = reflect(light_dir, vs_out_norm); 

	float si = pow( clamp( dot(viewDir, reflectDir), 0.0f, 1.0f ), specular_power );
	vec3 specular = Ls*Ms*si;

	vec4 textureColor =  texture(randomTexture, vs_out_tex0);
	fs_out_col = vec4(ambient + diffuse + specular, 1) * textureColor;
}