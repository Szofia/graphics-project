#pragma once

//
// Imported Mesh Object (from .OBJ)
//

#include <GL/glew.h>

#include <vector>
#include <glm/glm.hpp>

class Mesh
{
public:
	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texcoord;
	};

	Mesh(void);
	~Mesh(void);

	void InitBuffers();
	void Draw();

	void addVertex(const Vertex& vertex) {
		vertices.push_back(vertex);
	}

	void addIndex(unsigned int index) {
		indices.push_back(index);
	}

private:
	GLuint vertexArrayObject;
	GLuint vertexBuffer;
	GLuint indexBuffer;

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	bool inited = false;
};
