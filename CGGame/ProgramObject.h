#pragma once

// STL
#include <initializer_list>
#include <unordered_map>

// GLEW
#include <GL\glew.h>


#include "Conversions.hpp"

class ProgramObject final {
public:
	using AttributePair = std::pair<int, const char*>;
	ProgramObject(void);
	~ProgramObject(void);

	ProgramObject& AttachShader(std::initializer_list<GLuint> _shader_list);
	void BindAttributeLocations(std::initializer_list<AttributePair> _location_list);
	void LinkProgram();
	void Use();
	void Unuse();
	void Clean();

	GLint ResolveUniformLocation(GLint _uniform);
	GLint ResolveUniformLocation(const char * _uniform);

	GLint GetUniformLocation(const char * _uniform);
	
	template<typename U, typename T>
	void SetUniform(U _uniform, const T& pArr);

	void SetTexture(const char* _uniform, int _sampler, GLuint _textureID);

private:
	GLuint program_id_;
	std::vector<GLuint> attached_shaders_;
	std::unordered_map<std::string, GLint> uniform_locations_;
};

#include "ProgramObject.inl"

