#pragma once

//
// GL Utils
//

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdio.h>

#include <GL/glew.h>


inline GLuint loadShader(GLenum _shaderType, const char* _fileName) {
	
	// create shader + error handling
	GLuint shader = glCreateShader(_shaderType);

	if (shader == 0) {
		fprintf(stderr, "[Creating Shader] Error: Unable to create shader with type: %d; on filename: %s", _shaderType, _fileName);
		return 0;
	}

	// load shader from file + error handling
	std::string shaderCode = "";
	std::ifstream shaderStream(_fileName);

	if (!shaderStream.is_open()) {
		fprintf(stderr, "[Creating Shader] Error: Cannot load shader form file: %s", _fileName);
		return 0;
	}

	std::string line = "";
	while (std::getline(shaderStream, line)) {
		shaderCode += line + "\n";
	}

	shaderStream.close();

	// compile shader + error handling
	const char* sourcePointer = shaderCode.c_str();
	glShaderSource(shader, 1, &sourcePointer, NULL);
	glCompileShader(shader);

	GLint compileStatus = GL_FALSE;
	int infoLogLength;

	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

	if ( GL_FALSE == compileStatus ) {
		std::vector<char> VertexShaderErrorMessage(infoLogLength);
		glGetShaderInfoLog(shader, infoLogLength, NULL, &VertexShaderErrorMessage[0]);

		fprintf(stdout, "[Compile Shader] Error: Cannot compile shader on %s; Status: %s\n", _fileName, &VertexShaderErrorMessage[0]);
	}

	return shader;
}

GLuint generateRandomTexture() {

	const int width = 128, height = 128;
	unsigned char tex[width][height][3];

	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			tex[i][j][0] = rand() % 256;
			tex[i][j][1] = rand() % 256;
			tex[i][j][2] = rand() % 256;
		}
	}

	// generate texture on GPU
	GLuint texID;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGB,
		width,
		height,
		0,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		tex
	);

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	return texID;
}