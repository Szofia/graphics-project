#include "ProgramObject.h"

ProgramObject::ProgramObject(void) {
	program_id_ = glCreateProgram();
}

ProgramObject::~ProgramObject(void) {
	if (program_id_ != 0) glDeleteProgram(program_id_);
	Clean();
}

ProgramObject& ProgramObject::AttachShader(std::initializer_list<GLuint> _shader_list) {
	for( const int& shader : _shader_list) {
		glAttachShader(program_id_, shader);
		attached_shaders_.push_back(shader);
	}
	return *this;
}

void ProgramObject::BindAttributeLocations(std::initializer_list<AttributePair> _location_list) {
	for (const AttributePair& attribute_pair : _location_list) {
		glBindAttribLocation(program_id_, attribute_pair.first, attribute_pair.second);
	}
}

void ProgramObject::LinkProgram() {
	glLinkProgram(program_id_);

	// Error handling
	GLint link_status = GL_FALSE;
	int info_log_length;

	glGetProgramiv(program_id_, GL_LINK_STATUS, &link_status);
	glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_log_length);

	if (GL_FALSE == link_status) {
		std::vector<char> error_message(info_log_length);
		glGetShaderInfoLog(program_id_, info_log_length, NULL, &error_message[0]);

		fprintf(stdout, "[Link Program] Error: Cannot link program; Status: %s\n", &error_message[0]);
	}
}

void ProgramObject::Use() {
	glUseProgram(program_id_);
}

void ProgramObject::Unuse() {
	glUseProgram(0);
}
void ProgramObject::Clean(){
for (auto shader : attached_shaders_)
	{
		glDetachShader(program_id_, shader);
	}
}
GLint ProgramObject::ResolveUniformLocation(GLint _uniform) {
	return _uniform;
}

GLint ProgramObject::ResolveUniformLocation(const char* _uniform) {
	return GetUniformLocation(_uniform);
}

GLint ProgramObject::GetUniformLocation(const char * _uniform) {
	auto loc_it = uniform_locations_.find(_uniform);
	if (loc_it == uniform_locations_.end())
	{
		GLint curr_loc = glGetUniformLocation(program_id_, _uniform);
		uniform_locations_[_uniform] = curr_loc;
		return curr_loc;
	}
	else
		return loc_it->second;
}

void ProgramObject::SetTexture(const char * _uniform, int _sampler, GLuint _texture_id) {
	glActiveTexture(GL_TEXTURE0 + _sampler);
	glBindTexture(GL_TEXTURE_2D, _texture_id);
	glUniform1i(GetUniformLocation(_uniform), _sampler);
}
