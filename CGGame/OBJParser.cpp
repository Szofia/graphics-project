#include "ObjParser.h"

#include <string>

using namespace std;

Mesh* ObjParser::Parse(const char* fileName)
{
	ObjParser theParser;

	theParser.ifs.open(fileName, ios::in | ios::binary);
	if (!theParser.ifs)
		throw(EXC_FILENOTFOUND);

	theParser.mesh = new Mesh();

	while (theParser.SkipCommentLine())
	{
		if (false == theParser.ProcessLine())
			break;
	}

	theParser.ifs.close();

	return theParser.mesh;
}

bool ObjParser::ProcessLine()
{
	string line_id;
	float x, y, z;

	if (!(ifs >> line_id))
		return false;

	if ("v" == line_id) {	//	vertex data
		ifs >> x >> y >> z;
		positions.push_back(glm::vec3(x, y, z));
	}
	else if ("vt" == line_id) {	// texture data
		ifs >> x >> y;
		texcoords.push_back(glm::vec2(x, y));
	}
	else if ("vn" == line_id) {	// normal data
		ifs >> x >> y >> z;
		if (!ifs.good()) {                     // in case it is -1#IND00
			x = y = z = 0.0;
			ifs.clear();
			SkipLine();
		}
		normals.push_back(glm::vec3(x, y, z));
	}
	else if ("f" == line_id) {
		unsigned int iPosition = 0, iTexCoord = 0, iNormal = 0;

		for (unsigned int iFace = 0; iFace < 3; iFace++)
		{
			ifs >> iPosition;
			if ('/' == ifs.peek())
			{
				ifs.ignore();

				if ('/' != ifs.peek())
				{
					ifs >> iTexCoord;
				}

				if ('/' == ifs.peek())
				{
					ifs.ignore();

					// Optional vertex normal
					ifs >> iNormal;
				}
			}

			AddIndexedVertex(IndexedVert(iPosition - 1, iTexCoord - 1, iNormal - 1));
		}
	}
	else
		SkipLine();
	return true;
}

void ObjParser::AddIndexedVertex(const IndexedVert& vertex)
{
	if (vertexIndices[vertex] == 0) // new vertex
	{
		Mesh::Vertex v;
		v.position = positions[vertex.v];
		if (vertex.vt != -1)
			v.texcoord = texcoords[vertex.vt];
		if (vertex.vn != -1)
			v.normal = normals[vertex.vn];

		mesh->addVertex(v);
		mesh->addIndex(nIndexedVerts++);		// 0 based indices
		vertexIndices[vertex] = nIndexedVerts;	// but here 1 based, 0 signals "new one"
	}
	else {
		mesh->addIndex(vertexIndices[vertex] - 1);
	}
}

bool ObjParser::SkipCommentLine()
{
	char next;
	while (ifs >> std::skipws >> next)
	{
		ifs.putback(next);
		if ('#' == next)
			SkipLine();
		else
			return true;
	}
	return false;
}

void ObjParser::SkipLine()
{
	char next;
	ifs >> std::noskipws;
	while ((ifs >> next) && ('\n' != next));
}