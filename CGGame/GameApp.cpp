#include "GameApp.h"
#include "GLUtils.hpp"
#include "ParSurface.hpp"

#include <math.h>

GameApp::GameApp(void) {
	camera.SetView(glm::vec3(0, 0, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	m_vaoID = 0;
	m_vboID = 0;
	m_ibID = 0;
	m_randTexID = 0;
}

GameApp::~GameApp(void) { }

void GameApp::InitSphere() {

	Vertex sphere[(N + 1) * (M + 1)];
	for (int i = 0; i < N + 1; i++) {
		for (int j = 0; j < M + 1; ++j) {
			float u = i / (float)N;
			float v = j / (float)M;

			sphere[i + j * (N + 1)].p = getSphericalPos(u, v, 2);
			sphere[i + j * (N + 1)].n = getSphericalNorm(u, v);
			sphere[i + j * (N + 1)].t = getSphericalTex(u, v);
		}
	}

	GLushort sphereIndices[2 * 3 * M * N];
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < M; ++j) {
			sphereIndices[6 * i + j * 6 * N + 0] = (i) + (j) * (N + 1);
			sphereIndices[6 * i + j * 6 * N + 1] = (i + 1) + (j) * (N + 1);
			sphereIndices[6 * i + j * 6 * N + 2] = (i) + (j + 1) * (N + 1);
			sphereIndices[6 * i + j * 6 * N + 3] = (i + 1) + (j) * (N + 1);
			sphereIndices[6 * i + j * 6 * N + 4] = (i + 1) + (j + 1) * (N + 1);
			sphereIndices[6 * i + j * 6 * N + 5] = (i) + (j + 1) * (N + 1);
		}
	}

	// generate VAO, VBO, attach
	glGenVertexArrays(1, &m_vaoID);
	glBindVertexArray(m_vaoID);

	glGenBuffers(1, &m_vboID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(sphere),
		sphere,
		GL_STATIC_DRAW
	);

	// upload position oordinates
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		(GLuint)0,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		0
	);

	// upload normal coordinates
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		(GLuint)1,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(void*)(sizeof(glm::vec3))
	);

	// upload texture coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(
		(GLuint)2,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(void*)(2 * sizeof(glm::vec3))
	);
	
	// generate index buffer, upload
	glGenBuffers(1, &m_ibID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibID);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		sizeof(sphereIndices),
		sphereIndices,
		GL_STATIC_DRAW
	);

	// after uploading, detach VAO, VBO
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void GameApp::InitShaders() {
	// load shaders
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER, "vertexShader.vert");
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER, "fragmentShader.frag");

	// create program handling shaders
	program_.AttachShader({vs_ID, fs_ID});

	// attach shaders, bind attribute locations, link program
	program_.BindAttributeLocations({
		{0, "vs_in_position" },
		{1, "vs_in_norm" },	
		{2, "vs_in_tex0" }	
	});
	
	program_.LinkProgram();

	// free linked shaders
	glDeleteShader(vs_ID);
	glDeleteShader(fs_ID);
}

// Main Initialization Method
bool GameApp::Init() {
	// clear color: grey
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);

	InitShaders();

	// mesh
	mesh = ObjParser::Parse("Suzanne.obj");
	mesh -> InitBuffers();

	m_randTexID = generateRandomTexture();

	// create projection matrix
	camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

	return true;
}

// Clean buffers and program
void GameApp::Clean() {
	delete mesh;

	glDeleteTextures(1, &m_randTexID);

	glDeleteBuffers(1, &m_vboID);
	glDeleteBuffers(1, &m_vaoID);
	glDeleteBuffers(1, &m_ibID);

}

void GameApp::Update() {
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	camera.Update(delta_time);

	last_time = SDL_GetTicks();
}

// Render program
void GameApp::Render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// now we bind the VAO nad the program we created
	program_.Use();

	glm::mat4 viewProj = camera.GetViewProj();
	glm::vec3 eye = camera.GetEye();
	mWorld = glm::translate(glm::vec3(0, 2, 0));
	mWorldInv = glm::inverse(glm::transpose(mWorld));
	glm::mat4 mvp = viewProj * mWorld;


	program_.SetUniform("MVP", mvp);
	program_.SetUniform("World", mWorld);
	program_.SetUniform("viewPos", viewProj);
	program_.SetUniform("WorldInv", mWorldInv);

	// bind texture
	program_.SetTexture("randomTexture", GL_TEXTURE0, m_randTexID);

	glBindVertexArray(m_vaoID);

	mesh->Draw();

	// bind default values for further use
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	program_.Unuse();
}

void GameApp::KeyboardDown(SDL_KeyboardEvent& key) {

	camera.KeyboardDown(key);
}

void GameApp::KeyboardUp(SDL_KeyboardEvent& key) {

	camera.KeyboardUp(key);
}

void GameApp::MouseMove(SDL_MouseMotionEvent& mouse) {
	camera.MouseMove(mouse);
}

void GameApp::MouseDown(SDL_MouseButtonEvent& mouse) {

}

void GameApp::MouseUp(SDL_MouseButtonEvent& mouse) {

}

void GameApp::MouseWheel(SDL_MouseWheelEvent& wheel) {

}

void GameApp::Resize(int _w, int _h) {
	glViewport(0, 0, _w, _h);
	camera.Resize(_w, _h);
}