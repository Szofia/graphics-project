template<typename U, typename T>
void ProgramObject::SetUniform(U _uniform, const T& _p_arr) {
	using ElementType = typename ElementInfo<T>::value_type;
	using PrimitiveType = typename ExtractPrimitiveType<ElementType>::primitive_type;
	constexpr std::pair<size_t, size_t> component_count = ComponentCount<ElementType>();

	CallSetter<PrimitiveType, component_count.first, component_count.second>(
		ResolveUniformLocation(_uniform),
		ContainerLength(_p_arr),
		(const PrimitiveType*)PointerToStart(_p_arr)
		);
}