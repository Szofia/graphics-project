#include <iostream>
#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <math.h>

Camera::Camera(void) : eye(0.0f, 20.0f, 20.0f), at(0.0f), up(0.0f, 1.0f, 0.0f), m_speed(16.0f), goFw(0), goRight(0), m_slow(false)
{
	SetView(glm::vec3(0, 20, 20), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	camDist = glm::length(at - eye);

	SetProj(45.0f, 640 / 480.0f, 0.001f, 1000.0f);
}

Camera::Camera(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up) : m_speed(16.0f), goFw(0), goRight(0), camDist(10), m_slow(false)
{
	SetView(_eye, _at, _up);
}

Camera::~Camera(void)
{
}

void Camera::SetView(glm::vec3 _eye, glm::vec3 _at, glm::vec3 _up)
{
	eye = _eye;
	at = _at;
	up = _up;

	camViewDir = glm::normalize(at - eye);
	camRight = glm::normalize(glm::cross(camViewDir, up));

	camDist = glm::length(at - eye);

	camU = atan2f(camViewDir.z, camViewDir.x);
	camV = acosf(camViewDir.y);
}

void Camera::SetProj(float _angle, float _aspect, float _zn, float _zf)
{
	m_matProj = glm::perspective(_angle, _aspect, _zn, _zf);
	m_matViewProj = m_matProj * m_viewMatrix;
}

glm::mat4 Camera::GetViewMatrix()
{
	return m_viewMatrix;
}

void Camera::Update(float _deltaTime)
{
	eye += (goFw*camViewDir + goRight * camRight)*m_speed*_deltaTime;
	at += (goFw*camViewDir + goRight * camRight)*m_speed*_deltaTime;

	m_viewMatrix = glm::lookAt(eye, at, up);
	m_matViewProj = m_matProj * m_viewMatrix;
}

void Camera::UpdateUV(float du, float dv)
{
	camU += du;
	camV = glm::clamp<float>(camV + dv, 0.1f, 3.1f);

	at = eye + camDist * glm::vec3(cosf(camU)*sinf(camV),
		cosf(camV),
		sinf(camU)*sinf(camV));

	camViewDir = glm::normalize(at - eye);
	camRight = glm::normalize(glm::cross(camViewDir, up));
}

void Camera::SetSpeed(float _val)
{
	m_speed = _val;
}

void Camera::Resize(int _w, int _h)
{
	m_matProj = glm::perspective(45.0f, _w / (float)_h, 0.01f, 1000.0f);

	m_matViewProj = m_matProj * m_viewMatrix;
}

void Camera::KeyboardDown(SDL_KeyboardEvent& key)
{
	switch (key.keysym.sym)
	{
	case SDLK_LSHIFT:
	case SDLK_RSHIFT:
		if (!m_slow)
		{
			m_slow = true;
			m_speed /= 4.0f;
		}
		break;
	case SDLK_w:
		goFw = 1;
		break;
	case SDLK_s:
		goFw = -1;
		break;
	case SDLK_a:
		goRight = -1;
		break;
	case SDLK_d:
		goRight = 1;
		break;
	}
}

void Camera::KeyboardUp(SDL_KeyboardEvent& key)
{
	float current_speed = m_speed;
	switch (key.keysym.sym)
	{
	case SDLK_LSHIFT:
	case SDLK_RSHIFT:
		if (m_slow)
		{
			m_slow = false;
			m_speed *= 4.0f;
		}
		break;
	case SDLK_w:
	case SDLK_s:
		goFw = 0;
		break;
	case SDLK_a:
	case SDLK_d:
		goRight = 0;
		break;
	}
}

void Camera::MouseMove(SDL_MouseMotionEvent& mouse)
{
	if (mouse.state & SDL_BUTTON_LMASK)
	{
		UpdateUV(mouse.xrel / 100.0f, mouse.yrel / 100.0f);
	}
}

void Camera::LookAt(glm::vec3 _at)
{
	SetView(eye, _at, up);
}

